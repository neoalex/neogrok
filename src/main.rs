use {
    neogrok::runner::*,
    anyhow::Result,

    std::{ env::var
         , sync::atomic::{ AtomicU64
                         , Ordering } },
    num_cpus::get,
    mimalloc::MiMalloc,

    tokio::runtime::Builder,
};

#[global_allocator]
static GLOBAL: MiMalloc = MiMalloc;

fn main() -> Result<()> {
    let workers = var("NEOGROK_WORKERS")
                  .map(|r| r.parse::<usize>().unwrap())
                  .unwrap_or(get());
    
    let current_worker = AtomicU64::new(0);
    let rt = Builder::new_multi_thread()
                    .worker_threads(workers)
                    .enable_all()
                    .thread_name_fn(move || format!("NeoGrok Worker #{}", current_worker.fetch_add(
                        1,
                        Ordering::Relaxed
                    )))
                    .build()?;

    rt.block_on(run_neogrok())
}
