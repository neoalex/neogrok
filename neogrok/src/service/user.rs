use {
    neogrok_codec::{
        protocol::prelude::*,
    },

    config::*,
};

macro_rules! policy_flags {
    ($e:expr => $flag:expr) => {
        if $e {
            $flag
        } else {
            0
        }
    };
}

pub struct UserService {
    rights: u8
}

impl UserService {
    pub fn allowed_to(&self, rights: u8) -> bool {
        (self.rights & rights) == rights
    }

    pub fn grant(&mut self, rights: u8) -> u8 {
        self.rights |= rights;
        self.rights
    }

    pub fn reject(&mut self, rights: u8) -> u8{
        self.rights &= !rights;
        self.rights
    }

    pub fn new(rights: u8) -> Self {
        Self { rights }
    }
}

pub trait FromPolicies {
    fn from_policies(policy: &ServicePolicies) -> Self;
}

impl FromPolicies for u8 {
    fn from_policies(policy: &ServicePolicies) -> Self {
        let mut rights = 0;

        rights |= policy_flags!(policy.tcp.create => CAN_CREATE_TCP);
        rights |= policy_flags!(policy.tcp.select => CAN_SELECT_TCP);

        rights |= policy_flags!(policy.udp.create => CAN_CREATE_UDP);
        rights |= policy_flags!(policy.udp.select => CAN_SELECT_UDP);

        rights |= policy_flags!(policy.http.create => CAN_CREATE_HTTP);
        rights |= policy_flags!(policy.http.select => CAN_SELECT_HTTP);

        rights
    }
}

impl From<&ServicePolicies> for UserService {
    fn from(policy: &ServicePolicies) -> Self {
        Self::new(u8::from_policies(policy))
    }
}
