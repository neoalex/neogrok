use {
    pipeline::{
        consumer::PipelineConsumer,
        ext::*,

        async_trait
    },

    std::{
        net::{
            SocketAddr,
        },

        intrinsics::{ unlikely, likely },
    },
    tokio::{
        sync::mpsc::UnboundedReceiver,
        net::TcpListener,
        select,
        spawn,
    },
    safe_init::*,

    neogrok_codec::{
        protocol::prelude::*,
        writer::*,
    },
    config::*,

    crate::{
        proxy::{
            holder::*,
            frame::*,

            listener::*,
        },
        service::{
            user::*,
        },

        forever_waiter::run_forever,
    },

    anyhow::{ Result, anyhow },
};

pub struct ServerConsumer {
    address: SocketAddr,

    holder: SafeUninit![ProxyHolder],
    writer: CodecWriter<Writer>,

    config: SharedConfig,
    user: UserService,
}

impl ServerConsumer {
    pub async fn handle_network(
        &mut self,
        frame: NFrame
    ) -> Result<()> {
        loop { match frame {
            NFrame::Disconnect { id } if likely(self.holder.initialized()) => {
                match self.holder.disconnect(id) {
                    SendErrorKind::NoSuchClient => {
                        log::error!(
                            "{} (Disconnect) Error: No such client (ID#{})",
                            self.address,
                            id
                        );

                        self.writer.write_error(NO_SUCH_CLIENT)
                                   .await?;
                    },

                    _ => {}
                }
            },

            NFrame::Forward {
                id,
                buffer
            } if likely(self.holder.initialized()) => {
                let holder = self.holder.take_mut();
                match holder.forward(id, buffer) {
                    SendErrorKind::Closed => {
                        holder.disconnect(id);
                    },

                    SendErrorKind::NoSuchClient => {
                        log::error!("{} (Forward) Error: No such client (ID#{})", self.address, id);
                        self.writer.write_error(NO_SUCH_CLIENT).await?;
                    },

                    SendErrorKind::NoError => {}
                }
            },

            NFrame::Connect { id } if self.holder.initialized() => {
                log::error!(
                    "{} Sent Connect frame: unsupported on the server side (Tried ID#{})",
                    self.address,
                    id
                );
                self.writer.write_error(UNSUPPORTED_PKT).await?;
            },

            // Server creation related functions

            NFrame::Server { protocol, port } => {
                if unlikely(self.holder.initialized()) {
                    self.writer.write_error(SERVER_ALREADY_CREATED).await?;
                    break;
                }

                match protocol {
                    TransportProtocol::Tcp => {
                        if unlikely(!self.user.allowed_to(CAN_CREATE_TCP)) {
                            log::error!("{} Can't create TCP Server", self.address);

                            self.writer.write_error(ACCESS_DENIED).await?;
                            break;
                        } else if unlikely(port != 0 && !self.user.allowed_to(CAN_SELECT_TCP)) {
                            log::error!("{} Can't select TCP port ({})", self.address, port);

                            self.writer.write_error(ACCESS_DENIED).await?;
                            break;
                        }

                        let listener = match TcpListener::bind(
                            &format!("0.0.0.0:{}", port)
                        ).await {
                            Ok(l) => l,
                            Err(e) => {
                                log::error!("{} Can't bind port {}: {}", self.address, port, e);
                                break;
                            }
                        };

                        let address = listener.local_addr()?;
                        log::info!("{} Bound to {}", self.address, address);

                        let (
                            holder,
                            master,
                            token
                        ) = ProxyHolder::new();

                        self.holder.initialize(holder);
                        spawn(run_proxy_listener(
                            listener,
                            address.clone(),
                            master,
                            self.config.server.buffer_size.read,
                            token
                        ));
                        self.writer.write_port(address.port())
                                   .await?;
                    },

                    protocol => {
                        log::error!("{} Unimplemented protocol: {:?}", self.address, protocol);
                        self.writer.write_error(UNIMPLEMENTED).await?;
                    }
                }
            },

            NFrame::Authorize { magic } => {
                if &magic != self.config.authorization.magic.as_bytes() {
                    log::error!("{} Failed to authorize through magic", self.address);
                    self.writer.write_error(ACCESS_DENIED).await?;
                    break;
                }

                let rights = self.user.grant(
                    u8::from_policies(&self.config.permissions.magic)
                );
                self.writer.write_update_rights(rights)
                           .await?;
            },

            NFrame::Sync => {
                let compression = &self.config.compression;
                log::info!("{} Sync request - {:?}", self.address, compression);

                self.writer.write_sync_response(
                    compression.level,
                    compression.profit,
                    compression.threshold as u16
                ).await?;
            },

            NFrame::Ping => {
                log::info!("{} Ping request - {}", self.address, self.config.server.name);
                self.writer.write_ping_response(&self.config.server.name)
                           .await?;
            },

            NFrame::Error { code } => {
                log::error!("{} Error: {}", self.address, code.show_rights());

                self.writer.write_error(code).await?;
            },

            frame => {
                log::error!("{} state error: {:?}", self.address, frame);
                self.writer.write_error(STATE_ERROR)
                           .await?;
            },
        }; break; }
        Ok(())
    }

    pub async fn handle_slave(
        &mut self,
        frame: MasterFrame
    ) -> Result<()> {
        match frame {
            MasterFrame::Forward { id, buffer } => {
                self.writer.write_forward(id, buffer)
                           .await?;
            },

            MasterFrame::Connect { id, sink } => {
                self.holder.mount_client(id, sink);
                self.writer.write_connect(id)
                           .await?;
            },

            MasterFrame::Disconnect { id } => {
                self.holder.disconnect(id);
                self.writer.write_disconnect(id)
                           .await?;
            },

            #[allow(unreachable_patterns)]
            frame => {
                log::error!("{} Unhandled frame: {:?}", self.address, frame);
                return Err(anyhow!("Fatal channel error (Possibly disconnected)"));
            }
        }

        Ok(())
    }
}

#[async_trait]
impl PipelineConsumer for ServerConsumer {
    type Frame = NFrame;

    async fn run(
        mut self,
        mut stream: UnboundedReceiver<Self::Frame>
    ) -> Result<()> {
        loop {
            select! {
                network_frame = stream.recv() => if let Some(frame) = network_frame {
                    self.handle_network(frame)
                        .await?;
                } else {
                    break;
                },

                slave_frame = self.wait_slave_frame() => if let Some(frame) = slave_frame {
                    self.handle_slave(frame)
                        .await?;
                } else {
                    break;
                }
            }
        }

        Ok(())
    }
}

impl ServerConsumer {
    pub fn new(
        writer: Writer,
        address: SocketAddr,
        config: SharedConfig
    ) -> Self {
        Self { writer: CodecWriter::new( writer
                                       , config.compression.level
                                       , config.compression.profit
                                       , config.compression.threshold )
             , user: UserService::from(&config.permissions.join)
             , address
             , config
             , holder: SafeUninit::uninit() }
    }

    async fn wait_slave_frame(&mut self) -> Option<MasterFrame> {
        if self.holder.initialized() {
            self.holder
                .wait_frame()
                .await
        } else {
            run_forever().await;
            None
        }
    }
}

impl Drop for ServerConsumer {
    fn drop(&mut self) {
        if self.holder.initialized() {
            self.holder.shutdown();
            log::info!("{} Server shut down", self.address);
        }

        log::debug!("{} Consumer dropped", self.address);
    }
}
