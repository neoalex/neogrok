use {
    pipeline::{
        producer::*,
        ext::*,

        async_trait,
        Result,
    },

    tokio::{
        sync::mpsc::UnboundedSender
    },

    neogrok_codec::{
        protocol::frame::*,
        reader::*,
    },

    std::net::SocketAddr,
};

pub struct ServerProducer {
    address: SocketAddr,
    reader: Reader,
    max_payload_size: usize,
}

#[async_trait]
impl PipelineProducer for ServerProducer {
    type Frame = NFrame;

    async fn run(
        mut self,
        sink: UnboundedSender<Self::Frame>
    ) -> Result<()> {
        let mut codec_reader = CodecReader::server(
            &mut self.reader,
            self.max_payload_size,
        );

        loop {
            let frame = codec_reader.read_frame().await?;
            sink.send(frame)?;
        }
    }
}

impl ServerProducer {
    pub const fn new(
        reader: Reader,
        address: SocketAddr,
        max_payload_size: usize
    ) -> Self {
        Self { reader
             , max_payload_size
             , address }
    }
}

impl Drop for ServerProducer {
    fn drop(&mut self) {
        log::debug!("{} Producer dropped", self.address);
        log::info!("{} disconnected", self.address);
    }
}
