use {
    std::{
        future::Future,
        pin::Pin,
        task::{ Context
              , Poll },
    }
};

pub struct WaitForeverFuture;
impl Future for WaitForeverFuture {
    type Output = ();

    fn poll(
        self: Pin<&mut Self>,
        _cx: &mut Context<'_>
    ) -> Poll<Self::Output> {
        Poll::Pending
    }
}

pub fn run_forever() -> WaitForeverFuture {
    WaitForeverFuture{}
}
