use {
    anyhow::Result,

    config::*,
    std::{
        env::var,
    },

    fern::Dispatch,
    pipeline::{
        tcp::listener::*,
    },

    tokio::net::TcpListener,
    crate::factory::ServerFactory,
};

pub async fn run_neogrok() -> Result<()> {
    let config = if let Ok(path) = var("NEOGROK_CONFIG") {
        Config::try_load(path)?
    } else {
        Config::try_load_paths(&[
            "/etc/neogrok.ura",
            "neogrok.ura",

            "/etc/neogrok/server.ura",
            "/etc/neogrok/neogrok.ura",
            "/etc/neogrok/config.ura"
        ])?
    };

    let mut dispatcher = Dispatch::new();
    dispatcher = dispatcher.format(|out, message, record| {
        let level = record.level();
        let target = record.target();

        match level {
            log::Level::Debug => {
                out.finish(format_args!(
                    "NeoGrok:{} [{}]# {}",
                    record.level(),
                    target,
                    message
                ))
            },


            level => {
                out.finish(format_args!(
                    "NeoGrok:{}# {}",
                    level,
                    message
                ))
            }
        }
        
    });

    dispatcher = dispatcher.chain(std::io::stdout());
    for chain in config.logger.chain.iter() {
        dispatcher = dispatcher.chain(fern::log_file(chain)?);
    }

    dispatcher.level(config.logger.level.into())
              .apply()?;

    log::debug!("Running in debug mode");
    
    let listener = bind(&config.server).await?;
    let factory = ServerFactory::new(config);
    
    let tcp_pipeline = TcpNetworkPipelineListener::new(
        factory,
        listener,
    )?;
    
    tcp_pipeline.run()
                .await
}

async fn bind(
    server: &ServerConfig,
) -> Result<TcpListener> {
    let listener = TcpListener::bind(
        &server.listen
    ).await?;

    log::info!(
        "Started {} on {}",
        server.name,
        server.listen
    );

    Ok(listener)
}
