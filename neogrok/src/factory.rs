use {
    pipeline::{
        ext::*,
        factory::*,
        pair::*
    },

    crate::{
        pipeline::{
            producer::*,
            consumer::*,
        },
    },
    std::{ net::SocketAddr
         , sync::Arc },

    config::*,
};

pub struct ServerFactory {
    config: SharedConfig,
}

impl PipelineFactory for ServerFactory {
    type Consumer = ServerConsumer;
    type Producer = ServerProducer;

    fn buffer_size(&self) -> BufferSize {
        self.config.server
                   .buffer_size
                   .clone()
    }

    fn create_producer_consumer(
        &self,
        reader: Reader,
        writer: Writer,
        address: SocketAddr,
    ) -> ProducerConsumerPair<Self::Producer, Self::Consumer> {
        ProducerConsumerPair {
            producer: ServerProducer::new(
                reader,
                address.clone(),
                self.config.server.max_payload_size
            ),
            consumer: ServerConsumer::new(
                writer,
                address,
                Arc::clone(&self.config),
            )
        }
    }
}

impl ServerFactory {
    pub fn new(config: Config) -> Self {
        Self { config: config.into() }
    }
}
