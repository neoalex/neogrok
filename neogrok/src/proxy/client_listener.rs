use {
    std::{
        net::SocketAddr,
        intrinsics::unlikely
    },

    tokio::{
        net::TcpStream,
        sync::{ mpsc::{ UnboundedSender
                      , UnboundedReceiver } },
        io::{ AsyncReadExt
            , AsyncWriteExt },
        select,
    },

    anyhow::Result,
    crate::{
        proxy::{
            frame::*,
        }
    },

    idpool::*,
};

pub async fn run_proxy_client(
    creator: SocketAddr,
    address: SocketAddr,

    mut stream: TcpStream,

    master: UnboundedSender<MasterFrame>,
    mut slave: UnboundedReceiver<SlaveFrame>,

    buffer_length: usize,
    id: ClientId,
) -> Result<()> {
    let mut buffer = vec![0; buffer_length];
    let mut forcibly = false;

    loop {
        select! {
            received = stream.read(&mut buffer) => {
                let received = received.unwrap_or(0);
                if unlikely(received == 0) {
                    break;
                }

                let frame = MasterFrame::Forward { id
                                                 , buffer: Vec::from(&buffer[..received]) };
                if unlikely(master.send(frame).is_err()) {
                    break;
                }
            },

            frame = slave.recv() => {
                if let Some(frame) = frame {
                    match frame {
                        SlaveFrame::Forward { buffer } => if unlikely(stream.write_all(&buffer).await.is_err()) {
                            break;
                        },

                        SlaveFrame::ForceDisconnect => {
                            log::info!(
                                "{} is forcibly disconnected from {}'s server",
                                address,
                                creator,
                            );
                            forcibly = true;

                            break;
                        }
                    }
                } else {
                    break;
                }
            }
        }
    }

    if !forcibly {
        master.send(MasterFrame::Disconnect { id })
              .unwrap_or_default();
    }

    Ok(())
}
