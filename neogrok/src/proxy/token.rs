use {
    tokio::{
        sync::oneshot::{ Receiver
                       , Sender
                       , channel }
    },

    std::mem,
};

struct Token;
pub struct ShutdownTokenTx {
    inner: Option<Sender<Token>>,
}

pub struct ShutdownTokenRx {
    inner: Receiver<Token>
}

impl ShutdownTokenRx {
    pub async fn wait(
        &mut self
    ) {
        (&mut self.inner).await
                         .unwrap_or(Token{});
    }
}

impl ShutdownTokenTx {
    pub fn active(&self) -> bool {
        self.inner.is_some()
    }

    pub fn wake_by_ref(&mut self) {
        mem::replace(&mut self.inner, None)
           .unwrap()
           .send(Token{})
           .unwrap_or_default();
    }

    pub fn wake(self) {
        self.inner.unwrap()
                  .send(Token{})
                  .unwrap_or_default();
    }
}

pub fn shutdown_token() -> (ShutdownTokenTx, ShutdownTokenRx) {
    let (tx, rx) = channel();

    (
        ShutdownTokenTx { inner: Some(tx) },
        ShutdownTokenRx { inner: rx }
    )
}
