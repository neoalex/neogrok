use {
    idpool::ClientId,
    tokio::{
        sync::mpsc::UnboundedSender
    }
};

#[derive(Debug)]
pub enum SlaveFrame {
    Forward {
        buffer: Vec<u8>,
    },

    ForceDisconnect,
}

#[derive(Debug)]
pub enum MasterFrame {
    Connect {
        id: ClientId,
        sink: UnboundedSender<SlaveFrame>
    },

    Forward {
        id: ClientId,
        buffer: Vec<u8>,
    },

    Disconnect {
        id: ClientId,
    }
}
