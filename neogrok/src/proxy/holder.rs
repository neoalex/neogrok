use {
    tokio::{
        sync::{
            mpsc::{ UnboundedReceiver
                  , UnboundedSender
                  , unbounded_channel }
        }
    },

    rustc_hash::FxHashMap,
    super::{
        frame::*,
        token::*,
    },

    idpool::*,
    std::{
        future::Future,
    }
};

#[derive(Debug, PartialEq, Eq)]
pub enum SendErrorKind {
    NoSuchClient,
    Closed,

    NoError,
}

pub struct ProxyHolder {
    receiver: UnboundedReceiver<MasterFrame>,
    clients: FxHashMap<ClientId, UnboundedSender<SlaveFrame>>,
    token: ShutdownTokenTx,
}

impl ProxyHolder {
    pub fn wait_frame(
        &mut self
    ) -> impl Future<Output = Option<MasterFrame>> + '_ {
        self.receiver
            .recv()
    }

    pub fn shutdown(&mut self) -> bool {
        if self.token.active() {
            self.token.wake_by_ref();
            true
        } else {
            false
        }
    }

    pub fn disconnect(
        &mut self,
        id: ClientId
    ) -> SendErrorKind {
        if let Some(sink) = self.clients.remove(&id) {
            sink.send(SlaveFrame::ForceDisconnect)
                .unwrap_or_default();
            SendErrorKind::NoError
        } else {
            SendErrorKind::NoSuchClient
        }
    }

    pub fn forward(
        &self,
        id: ClientId,
        buffer: Vec<u8>
    ) -> SendErrorKind {
        self.send(
            id,
            SlaveFrame::Forward { buffer }
        )
    }

    pub fn send(
        &self,
        id: ClientId,
        frame: SlaveFrame
    ) -> SendErrorKind {
        if let Some(sink) = self.clients.get(&id) {
            if sink.send(frame).is_ok() {
                SendErrorKind::NoError
            } else {
                SendErrorKind::Closed
            }
        } else {
            SendErrorKind::NoSuchClient
        }
    }

    pub fn mount_client(
        &mut self,
        id: ClientId,
        sink: UnboundedSender<SlaveFrame>
    ) {
        self.clients
            .insert(id, sink);
    }

    pub fn new() -> (
        Self,
        UnboundedSender<MasterFrame>,
        ShutdownTokenRx,
    ) {
        let (tx, rx) = unbounded_channel();
        let (stx, srx) = shutdown_token();

        let holder = ProxyHolder {
            receiver: rx,
            clients: Default::default(),
            token: stx
        };

        ( holder
        , tx
        , srx )
    }
}
