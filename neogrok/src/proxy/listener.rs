use {
    tokio::{
        net::{ TcpListener },
        sync::{ mpsc::{ UnboundedSender
                      , unbounded_channel } },

        spawn,
        select,
    },

    crate::{
        proxy::{
            frame::*,
            client_listener::*,
            token::*,
        }
    },

    std::{
        net::SocketAddr,
        sync::Arc,
    },

    anyhow::Result,
    idpool::*,
};

pub async fn run_proxy_listener(
    listener: TcpListener,
    creator: SocketAddr,
    master: UnboundedSender<MasterFrame>,

    buffer_length: usize,
    mut token: ShutdownTokenRx,
) -> Result<()> {
    let pool: SharedIDPool = IDPool::zero().into();

    loop {
        select! {
            biased;

            _ = token.wait() => {
                break;
            }

            result = listener.accept() => {
                let (tcp_stream, address) = if let Ok(l) = result {
                    l
                } else {
                    break;
                };
                let id = pool.request_id().await;

                log::info!(
                    "{} is connected to the {}'s proxy (ID#{})",
                    address,
                    creator,
                    id
                );

                let (sink, stream) = unbounded_channel();
                let frame = MasterFrame::Connect { id, sink };

                if master.send(frame).is_err() {
                    break;
                }

                let pool = Arc::clone(&pool);
                let master = master.clone();

                spawn(async move {
                    run_proxy_client(
                        creator,
                        address,
                        tcp_stream,
                        master,
                        stream,
                        buffer_length,
                        id
                    ).await
                    .unwrap_or_default();

                    pool.return_id(id).await;
                });
            }
        }
    }

    Ok(())
}
