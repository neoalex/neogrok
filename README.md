# NeoGrok

Effective and Open Source NGrok alternative written in Rust

# Compilation

Download Rust language compiler to your computer using [rustup](https://rustup.rs/) and compile it simply by executing the following command:

```bash
$ cargo build --release # Build server release
$ sudo mv target/release/neogrokd /usr/bin # Move neogrokd to the specific directory, for example it will be /usr/bin
$ rm -rf target   # Clean up build directory

$ sudo mv neogrok.yaml /etc/  # Move config to the /etc directory
```

# Configuration

For configuration format see [Gura](https://gura.netlify.app/)

See [neogrok.ura](./neogrok.ura) for example configuration with fields explained

# Features

Some features are planned and will be implemented ASAP.

- [x] Network pipeline implementation
- [ ] Database related functions
  - [ ] Authorization
- [x] Scalable
  - [x] Specify workers count
- [x] TCP port forwarding
- [x] Packet compression
  - [ ] Implement compression algorithm selection
  - [x] Zstd
    - [ ] Loading dictionaries
  - [ ] Deflate
- [ ] UDP port forwarding support
  - [ ] Peer to Peer protocol
- [ ] Application level protocols implemented
  - [ ] HTTP
  - [ ] SSH
- [x] Highly configurable
